import random


# 定义一个随机选择器类
class RandomSelector:
    # 初始化方法，设置初始值，读取文件内容到列表，并初始化随机数生成器
    def __init__(self):
        self.last_list = None  # 存储上一次抽取的结果列表
        self.name_list_from_file = self.read_file_to_list()  # 从文件中读取名字列表
        self.name_list_no_jp = [
            item for item in self.name_list_from_file if not item.endswith("*日")
        ]
        random.seed()  # 设置随机数种子以确保结果的随机性

    # 过滤列表A中的元素不在列表B中的元素
    def filter_lists(self, A: list, B: list):
        B_set = set(B)  # 将B转换为集合以提高查找效率
        outlist = [i for i in A if i not in B_set]  # 筛选出不在B中的元素构成新列表
        return outlist

    # 从文件中读取内容并转化为列表
    def read_file_to_list(self):
        with open("抽取列表.txt", encoding="utf-8") as file:
            list_from_file = [
                line.strip()
                for line in file
                if not line.startswith("#")  # 添加判断条件，忽略注释行
            ]  # 去除每一行末尾换行符并存储为列表

        return list_from_file

    # 随机选取num个结果（不考虑排除上次已抽选的人）
    def get_random_result(self, num: int, isJP: bool):
        # 根据isJP标志，筛选出相应的名字列表
        if isJP:
            name_list = self.name_list_no_jp
        else:
            name_list = self.name_list_from_file
        random.shuffle(name_list)  # 对名字列表进行随机排序
        return name_list[:num]  # 返回前num个随机排序后的名字

    def get_random_result_rmlast(self, num: int, lastlist: list, isJP: bool):
        """
        随机选取num个结果，排除上次已抽选的人。

        参数:
        - num: int, 需要随机选取的名字数量。
        - lastlist: list, 上次抽选后记录的名字列表。
        - isJP: bool, 标识是否抽取日语班人的名字。

        返回值:
        - list, 包含随机选取的名字的列表。如果无法选取足够数量的名字，则返回所有剩余的名字。
        """
        # 根据isJP标志，筛选出相应的名字列表
        if isJP:
            name_list = self.name_list_no_jp
        else:
            name_list = self.name_list_from_file

        # 过滤掉上次已经抽选的名字
        if lastlist:
            name_list = self.filter_lists(name_list, lastlist)
        else:
            name_list = name_list.copy()

        # 对剩余的名字列表进行随机排序
        if len(name_list) != 0:
            random.shuffle(name_list)

            # 如果剩余名字数量大于等于需要抽取的数量，则返回前num个随机排序后的名字
            if len(name_list) >= num:
                last_list = name_list[:num]
                return name_list[:num]
            else:
                return name_list  # 若不够num个，则返回所有剩余的名字
        else:
            return [
                "已经抽完人了，请点击「重置排除」重置"
            ]  # 若无剩余名字可供抽取，则返回提示信息


if __name__ == "__main__":
    selector = RandomSelector()
    print(selector.get_random_result(2, 1))
